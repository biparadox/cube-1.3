#ifndef HTTP_FILESERVER_AGENT_H
#define HTTP_FILESERVER_AGENT_H

int http_fileserver_agent_init(void * sub_proc,void * para);
int http_fileserver_agent_start(void * sub_proc,void * para);

#endif
