#ifndef RECORD_FIND_H
#define RECORD_FIND_H
 
int record_find_init (void * sub_proc, void * para);
int record_find_start (void * sub_proc, void * para);
#endif
